---- drop ----
DROP TABLE IF EXISTS `users`;


CREATE TABLE IF NOT EXISTS users (
	`uuid` BINARY(16) PRIMARY KEY ,
	`name` VARCHAR(120) NOT  NULL,
	`age` INT(3) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`address` JSON NOT NULL,
	`created_at`       Datetime DEFAULT NULL,
    `updated_at`       Datetime DEFAULT NULL,
	CONSTRAINT users_UN UNIQUE KEY (email),
	INDEX (uuid)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
