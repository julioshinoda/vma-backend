# vma-backend

This is a REST API for a CRUD of users.

## Required

Installed docker and docker-compose. For running test need Go 1.14+

## Instructions

For run the project use command on project root **make run** or **docker-compose up**. Then a postgres database up with initial schema and a rest api made with golang run on port **9011**. 

For run test use command **make test**

## Endpoints

For security all endpoints are protected with token authentication. So, all requests needs a Bearer token. Example below

> Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8

You can use this token **eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8** or generate anohter running **make generate-token**

### Create user

Endpoint to create a new user

```curl
curl --verbose --header "Content-Type: application/json" \
 --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8" \
  --request POST \
  --data '{
    "name":"curl",
    "age":30,
    "email":"user@test.com.br",
    "password":"senha123",
    "address":{
        "street":"backer",
        "number":"qwq",
        "state":"asas",
        "city":"sasa",
        "zipcode":"assas"
    }

}' \
  http://localhost:9011/user
```

**Response**

Case success to create user return status 201 

Case an error occur return status 400 and error message. Example below

```json
{
  "error": "user already exists"
}
```

### Get users 

Endpoint to get users. 

Available query params

- **field** field used to order response. Possible values are age, name, email.
- **order** How start ordering.  Possible values are **ASC** and **DESC**.
- **limit** Limit of results that can be retrieve. If not set, use default of 10 users.


```curl
curl --verbose \
 --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8" \
  --request GET \
  http://localhost:9011/user?field=age&order=DESC
```

**Response**


```json
[
  {
    "uuid": "9ca95727-2c72-11eb-a3ef-0242c0a86002",
    "name": "test2",
    "age": 30,
    "email": "user2@test.com.br",
    "address": {
      "street": "backer",
      "number": "qwq",
      "state": "asas",
      "city": "sasa",
      "zipcode": "assas"
    }
  },
  {
    "uuid": "deeacc6c-2c71-11eb-a3ef-0242c0a86002",
    "name": "test",
    "age": 29,
    "email": "user1@test.com.br",
    "address": {
      "street": "backer",
      "number": "qwq",
      "state": "asas",
      "city": "sasa",
      "zipcode": "assas"
    }
  }
]
```

### Get user by uuid

Get an user by uuid

```curl
curl --verbose \
 --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8" \
  --request GET \
  http://localhost:9011/user/3e81946a-2c72-11eb-a3ef-0242c0a86002
```

**Response**

Case find an user

Status 200

```json
{
  "uuid": "3e81946a-2c72-11eb-a3ef-0242c0a86002",
  "name": "curl",
  "age": 30,
  "email": "user2@test.com.br",
  "address": {
    "street": "backer",
    "number": "qwq",
    "state": "asas",
    "city": "sasa",
    "zipcode": "assas"
  }
}
```

Case not find a user 

Status 400

```json
{
  "error": "user not found"
}
```

### DELETE user by uuid

Delete an user by uuid

```curl
curl --verbose \
 --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8" \
  --request DELETE \
  http://localhost:9011/user/3e81946a-2c72-11eb-a3ef-0242c0a86002
```

**Response**

Case success return status 200

Case error return status 400 

### Update an user

This endpoint update user info except password.

```curl
curl --verbose --header "Content-Type: application/json" \
 --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8" \
  --request PUT \
  --data '{
    "name":"curl",
    "age":30,
    "email":"user.updated@test.com.br",
    "address":{
        "street":"updated",
        "number":"updated",
        "state":"updated",
        "city":"updated",
        "zipcode":"updated"
    }

}' \
  http://localhost:9011/user/9ca95727-2c72-11eb-a3ef-0242c0a86002
```

**Response**

Case success return status 200

Case error return status 400 

### Update user password

This endpoint update user info except password.

```curl
curl --verbose --header "Content-Type: application/json" \
 --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMjN9.qHpsxTHK8l2D6jFLNln_WmknzOY-msXRN6XSxgnAID8" \
  --request PATCH \
  --data '{
    "current":"senha123",
    "new":"123"
}' \
  http://localhost:9011/user/password/9ca95727-2c72-11eb-a3ef-0242c0a86002
```

**Response**

Case success return status 200

Case error return status 400 and message

```json
{"error":"incorrect password"}
```

