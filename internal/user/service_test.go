package user

import (
	"errors"
	"os"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/julioshinoda/vma-backend/internal/user/mocks"
	"gitlab.com/julioshinoda/vma-backend/pkg/constants"
	"gitlab.com/julioshinoda/vma-backend/pkg/model"
)

func Test_service_Create(t *testing.T) {
	type fields struct {
		Repo Repository
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "[T0/Create] success to create user",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				user: model.User{
					Name:     "nome sobrenome",
					Email:    "nome.sobrenome@email.com",
					Age:      19,
					Password: "secret",
					Address: model.Address{
						Street:  "street x",
						Number:  "1a",
						State:   "neighbourhood",
						City:    "Manchester",
						Zipcode: "ABC 123",
					},
				},
			},
		},
		{
			name: "[T1/Create] invalid request to create user",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				user: model.User{
					Name:     "nome sobrenome",
					Email:    "nome.sobrenome",
					Age:      19,
					Password: "secret",
					Address: model.Address{
						Street:  "street x",
						Number:  "1a",
						State:   "neighbourhood",
						City:    "Manchester",
						Zipcode: "ABC 123",
					},
				},
			},
			wantErr: true,
		},
	}
	os.Setenv("SECRET_KEY", "a06f4978327e064301c407ed109de0cc53e7300c72d7dec2571a151fbd49a46e")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				Repo: tt.fields.Repo,
			}
			switch tt.name {
			case "[T0/Create] success to create user":
				s.Repo.(*mocks.Repository).On("Insert", mock.Anything).Return(nil)
			case "[T1/Create] invalid request to create user":

			}
			if err := s.Create(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("service.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_service_Get(t *testing.T) {
	type fields struct {
		Repo Repository
	}
	type args struct {
		options model.QueryOptions
	}
	users := []model.User{
		{
			Email: "test@vma.co.uk",
			Name:  "Test",
			Age:   12,
			Address: model.Address{
				Street:  "principal",
				Number:  "1",
				City:    "Gothan",
				State:   "Somewhere",
				Zipcode: "12312323",
			},
		},
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.User
		wantErr bool
	}{
		{
			name: "[T0/Get] success to get user",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				options: model.QueryOptions{},
			},
			want: users,
		},
		{
			name: "[T1/Get]  filter not allowed",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				options: model.QueryOptions{Order: model.QueryOrder{Field: "passaword"}},
			},
			want:    []model.User{},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				Repo: tt.fields.Repo,
			}
			switch tt.name {
			case "[T0/Get] success to get user":
				s.Repo.(*mocks.Repository).On("Get", mock.Anything).Return(users, nil)
			case "[T1/Get]  filter not allowed":
			}
			got, err := s.Get(tt.args.options)
			if (err != nil) != tt.wantErr {
				t.Errorf("service.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("service.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_service_Update(t *testing.T) {
	type fields struct {
		Repo Repository
	}
	type args struct {
		user model.User
	}
	new := model.User{
		Email: "test@vma.co.uk",
		Name:  "Test",
		Age:   12,
		Address: model.Address{
			Street:  "principal",
			Number:  "1",
			City:    "Gothan",
			State:   "Somewhere",
			Zipcode: "12312323",
		},
	}
	old := model.User{
		Email: "test@vma.co.uk",
		Name:  "Test",
		Age:   20,
		Address: model.Address{
			Street:  "changed street",
			Number:  "2",
			City:    "Gothan",
			State:   "Somewhere",
			Zipcode: "00099667",
		},
	}
	invalidUser := model.User{
		Email: "select * ",
		Name:  "Test",
		Age:   12,
		Address: model.Address{
			Street:  "principal",
			Number:  "1",
			City:    "Gothan",
			State:   "Somewhere",
			Zipcode: "12312323",
		},
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "[T0/Update] succes to update",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				user: new,
			},
		},
		{
			name: "[T1/Update] user not found",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				user: new,
			},
			wantErr: true,
		},
		{
			name: "[T2/Update] invalid body",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				user: invalidUser,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				Repo: tt.fields.Repo,
			}
			switch tt.name {
			case "[T0/Update] succes to update":
				s.Repo.(*mocks.Repository).On("GetByUUID", mock.Anything).Return(old, nil)
				new.Password = "not changed"
				s.Repo.(*mocks.Repository).On("Update", new).Return(nil)
			case "[T1/Update] user not found":
				s.Repo.(*mocks.Repository).On("GetByUUID", mock.Anything).Return(old, errors.New(constants.UserNotFound))
			case "[T2/Update] invalid body":
				s.Repo.(*mocks.Repository).On("GetByUUID", mock.Anything).Return(old, nil)
			}
			if err := s.Update(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("service.Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_service_UpdatePassword(t *testing.T) {
	type fields struct {
		Repo Repository
	}
	type args struct {
		req model.RequestNewPass
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "[T0/UpdatePassword] succes to update password",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				req: model.RequestNewPass{
					UUID:    "",
					New:     "test123",
					Current: "secret123",
				},
			},
		},
		{
			name: "[T1/UpdatePassword] validate request",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				req: model.RequestNewPass{
					UUID: "",
					New:  "test123",
				},
			},
			wantErr: true,
		},
		{
			name: "[T2/UpdatePassword] error on get saved password",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				req: model.RequestNewPass{
					UUID:    "",
					New:     "test123",
					Current: "secret123",
				},
			},
			wantErr: true,
		},
		{
			name: "[T3/UpdatePassword] error on decrypt",
			fields: fields{
				Repo: &mocks.Repository{},
			},
			args: args{
				req: model.RequestNewPass{
					UUID:    "",
					New:     "test123",
					Current: "wrong",
				},
			},
			wantErr: true,
		},
	}
	os.Setenv("SECRET_KEY", "c83c3c8682cc6f2bbdcc7858b74d493961b8c667f377b644df4f68acccba177c")
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service{
				Repo: tt.fields.Repo,
			}
			switch tt.name {
			case "[T0/UpdatePassword] succes to update password":
				s.Repo.(*mocks.Repository).On("GetPassword", mock.Anything).Return("9898a2c396dc11675a5b12c28f5b5675b2400442c91db5258ea90e4e92226122d884b9fdd9", nil)
				s.Repo.(*mocks.Repository).On("UpdatePassword", mock.Anything, mock.Anything).Return(nil)
			case "[T1/UpdatePassword] validate request":
			case "[T2/UpdatePassword] error on get saved password":
				s.Repo.(*mocks.Repository).On("GetPassword", mock.Anything).Return("", errors.New(constants.UserNotFound))
			case "[T3/UpdatePassword] error on decrypt":
				s.Repo.(*mocks.Repository).On("GetPassword", mock.Anything).Return("9898a2c396dc11675a5b12c28f5b5675b2400442c91db5258ea90e4e92226122d884b9fdd9", nil)
			}
			if err := s.UpdatePassword(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("service.UpdatePassword() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
