package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/julioshinoda/vma-backend/pkg/constants"
	"gitlab.com/julioshinoda/vma-backend/pkg/database"
	"gitlab.com/julioshinoda/vma-backend/pkg/database/mysql"
	"gitlab.com/julioshinoda/vma-backend/pkg/model"
)

type Repository interface {
	Insert(user model.User) error
	Get(options model.QueryOptions) ([]model.User, error)
	GetPassword(UUID string) (string, error)
	GetByUUID(UUID string) (model.User, error)
	Delete(UUID string) error
	Update(user model.User) error
	UpdatePassword(UUID, encryptedPass string) error
}

type repo struct {
	DB database.SQLInterface
}

func NewRepo() Repository {
	return repo{DB: mysql.GetDBConn()}
}

func (r repo) Insert(user model.User) error {
	addr, _ := json.Marshal(user.Address)
	input := database.QueryConfig{
		QueryStr: `INSERT INTO users (uuid,name,age,email,password,address)
			VALUES (UUID_TO_BIN(UUID()),?,?,?,?,?);`,
		Values: []interface{}{
			user.Name,
			user.Age,
			user.Email,
			user.Password,
			addr,
		},
	}
	_, err := r.DB.Query(input)
	if err != nil {
		if strings.Contains(err.Error(), "1062: Duplicate ") {
			return errors.New(constants.DuplicateUserError)
		}
		return err
	}
	return nil
}

func (r repo) Update(user model.User) error {
	addr, _ := json.Marshal(user.Address)
	input := database.QueryConfig{
		QueryStr: `UPDATE users SET name = ?,age = ?, email = ?, address = ? WHERE uuid = UUID_TO_BIN(?);`,
		Values: []interface{}{
			user.Name,
			user.Age,
			user.Email,
			addr,
			user.UUID,
		},
	}
	_, err := r.DB.Query(input)
	if err != nil {
		return err
	}
	return nil
}

func (r repo) UpdatePassword(UUID, encryptedPass string) error {

	input := database.QueryConfig{
		QueryStr: `UPDATE users SET password = ? WHERE uuid = UUID_TO_BIN(?);`,
		Values: []interface{}{
			encryptedPass,
			UUID,
		},
	}
	_, err := r.DB.Query(input)
	if err != nil {
		return err
	}
	return nil
}

func (r repo) Get(options model.QueryOptions) ([]model.User, error) {
	query := `SELECT BIN_TO_UUID(uuid) as uuid,name,age,email,address from users `

	if options.Order.Field != "" {
		query += fmt.Sprintf(" %s %s %s", "ORDER BY", options.Order.Field, options.Order.Order)
	}
	query += " LIMIT 10"
	if options.Limit > 0 {
		query = strings.Replace(query, "LIMIT 10", fmt.Sprintf("LIMIT %d", options.Limit), 1)
	}
	input := database.QueryConfig{
		QueryStr: query, //`SELECT BIN_TO_UUID(uuid) as uuid,name,age,email,address from users LIMIT 10;`
		Values:   []interface{}{},
	}
	response, err := r.DB.Query(input)
	if err != nil {
		return []model.User{}, err
	}
	users := []model.User{}
	for _, row := range response {
		age, _ := strconv.Atoi(string(row["age"].([]byte)))
		user := model.User{
			UUID:  string(row["uuid"].([]byte)),
			Age:   age,
			Name:  string(row["name"].([]byte)),
			Email: string(row["email"].([]byte)),
		}
		json.Unmarshal(row["address"].([]byte), &user.Address)
		users = append(users, user)
	}

	return users, err
}

func (r repo) GetByUUID(UUID string) (model.User, error) {
	input := database.QueryConfig{
		QueryStr: `SELECT name,age,email,address from users WHERE uuid = UUID_TO_BIN(?)`,
		Values:   []interface{}{UUID},
	}
	response, err := r.DB.Query(input)
	if err != nil {
		return model.User{}, err
	}
	user := model.User{}
	if len(response) == 0 {
		return model.User{}, errors.New(constants.UserNotFound)
	}
	for _, row := range response {
		user.Age = int(row["age"].(int64))
		user.UUID = UUID
		user.Name = string(row["name"].([]byte))
		user.Email = string(row["email"].([]byte))
		json.Unmarshal(row["address"].([]byte), &user.Address)
	}
	return user, err
}

func (r repo) GetPassword(UUID string) (string, error) {
	input := database.QueryConfig{
		QueryStr: `SELECT password from users WHERE uuid = UUID_TO_BIN(?)`,
		Values:   []interface{}{UUID},
	}
	response, err := r.DB.Query(input)
	if err != nil {
		return "", err
	}
	for _, row := range response {
		return string(row["password"].([]byte)), nil
	}
	return "", errors.New(constants.UserNotFound)
}

func (r repo) Delete(UUID string) error {
	input := database.QueryConfig{
		QueryStr: `DELETE FROM users  WHERE uuid = UUID_TO_BIN(?)`,
		Values:   []interface{}{UUID},
	}
	_, err := r.DB.Query(input)
	if err != nil {
		return err
	}
	return nil
}
