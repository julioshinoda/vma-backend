package user

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/julioshinoda/vma-backend/pkg/httphelper"
	"gitlab.com/julioshinoda/vma-backend/pkg/model"
)

func Handler(r chi.Router) {
	r.Post("/user", Create)
	r.Get("/user", GetUser)
	r.Put("/user/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}", Update)
	r.Patch("/user/password/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}", ChangePassword)
	r.Get("/user/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}", GetUserByUUID)
	r.Delete("/user/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}", DeleteByUUID)
}

func Create(w http.ResponseWriter, r *http.Request) {
	var request model.User
	json.NewDecoder(r.Body).Decode(&request)
	service := NewUser()
	err := service.Create(request)
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func Update(w http.ResponseWriter, r *http.Request) {
	var request model.User
	json.NewDecoder(r.Body).Decode(&request)
	request.UUID = chi.URLParam(r, "uuid")
	service := NewUser()
	err := service.Update(request)
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	var request model.User
	json.NewDecoder(r.Body).Decode(&request)
	queryOptions := model.QueryOptions{}
	if r.URL.Query().Get("field") != "" {
		queryOptions.Order.Field = strings.ToLower(r.URL.Query().Get("field"))
		if r.URL.Query().Get("order") != "" {
			queryOptions.Order.Order = strings.ToUpper(r.URL.Query().Get("order"))

		}
	}
	if r.URL.Query().Get("limit") != "" {
		limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
		if err != nil {
			w = httphelper.ErrorResponse(w, err)
			return

		}
		queryOptions.Limit = limit

	}
	service := NewUser()
	users, err := service.Get(queryOptions)
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	response, err := json.Marshal(users)
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
	w.WriteHeader(http.StatusOK)
}

func GetUserByUUID(w http.ResponseWriter, r *http.Request) {
	var request model.User
	json.NewDecoder(r.Body).Decode(&request)
	service := NewUser()
	users, err := service.GetByUUID(chi.URLParam(r, "uuid"))
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	response, err := json.Marshal(users)
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
	w.WriteHeader(http.StatusOK)
}

func DeleteByUUID(w http.ResponseWriter, r *http.Request) {
	service := NewUser()
	err := service.DeleteByUUID(chi.URLParam(r, "uuid"))
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	var request model.RequestNewPass
	json.NewDecoder(r.Body).Decode(&request)
	request.UUID = chi.URLParam(r, "uuid")
	service := NewUser()
	err := service.UpdatePassword(request)
	if err != nil {
		w = httphelper.ErrorResponse(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
}
