package user

import (
	"errors"
	"os"

	"github.com/imdario/mergo"
	"gitlab.com/julioshinoda/vma-backend/pkg/constants"
	"gitlab.com/julioshinoda/vma-backend/pkg/cypher"
	"gitlab.com/julioshinoda/vma-backend/pkg/model"
)

type Manager interface {
	Create(user model.User) error
	Update(user model.User) error
	Get(options model.QueryOptions) ([]model.User, error)
	GetByUUID(UUID string) (model.User, error)
	DeleteByUUID(UUID string) error
	UpdatePassword(req model.RequestNewPass) error
}

type service struct {
	Repo Repository
}

func NewUser() Manager {
	return service{Repo: NewRepo()}
}

func (s service) Create(user model.User) error {
	if err := user.Validate(); err != nil {
		return err
	}
	user.Password = cypher.Encrypt(user.Password, os.Getenv("SECRET_KEY"))
	return s.Repo.Insert(user)
}

func (s service) Update(user model.User) error {
	current, err := s.Repo.GetByUUID(user.UUID)
	if err != nil {
		return err
	}
	mergo.Merge(&user, current)
	user.Password = "not changed"
	if err := user.Validate(); err != nil {
		return err
	}
	return s.Repo.Update(user)
}

func (s service) UpdatePassword(req model.RequestNewPass) error {
	if err := req.Validate(); err != nil {
		return err
	}
	encryptedPass, err := s.Repo.GetPassword(req.UUID)
	if err != nil {
		return err
	}
	if cypher.Decrypt(encryptedPass, os.Getenv("SECRET_KEY")) != req.Current {
		return errors.New(constants.IncorrectPassword)
	}
	return s.Repo.UpdatePassword(req.UUID, cypher.Encrypt(req.New, os.Getenv("SECRET_KEY")))
}

func (s service) Get(options model.QueryOptions) ([]model.User, error) {
	if err := options.Validate(); err != nil {
		return []model.User{}, err
	}
	return s.Repo.Get(options)
}

func (s service) GetByUUID(UUID string) (model.User, error) {
	return s.Repo.GetByUUID(UUID)
}

func (s service) DeleteByUUID(UUID string) error {
	return s.Repo.Delete(UUID)
}
