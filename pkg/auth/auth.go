package auth

import (
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
)

func GenerateToken() string {
	tokenAuth := jwtauth.New("HS256", []byte(os.Getenv("JWT_SECRET")), nil)
	_, tokenString, _ := tokenAuth.Encode(jwt.MapClaims{"expires_at": time.Now().Add(time.Hour * 6).Unix()})
	return tokenString
}
