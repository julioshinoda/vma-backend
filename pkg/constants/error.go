package constants

const (
	DuplicateUserError = "user already exists"
	UserNotFound       = "user not found"
	IncorrectPassword  = "incorrect password"
)
