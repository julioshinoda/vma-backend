package model

import (
	"github.com/go-playground/validator/v10"
)

type User struct {
	UUID     string  `json:"uuid"`
	Name     string  `json:"name" validate:"required"`
	Age      int     `json:"age" validate:"gte=0,lte=110,required"`
	Email    string  `json:"email" validate:"required,email"`
	Password string  `json:"password,omitempty" validate:"required"`
	Address  Address `json:"address"`
}

type Address struct {
	Street  string `json:"street" validate:"required"`
	Number  string `json:"number" validate:"required"`
	State   string `json:"state" validate:"required"`
	City    string `json:"city" validate:"required"`
	Zipcode string `json:"zipcode" validate:"required"`
}

type RequestNewPass struct {
	UUID    string `json:"uuid"`
	Current string `json:"current" validate:"required"`
	New     string `json:"new" validate:"required"`
}

func (user User) Validate() error {
	validate := validator.New()
	return validate.Struct(user)
}

func (req RequestNewPass) Validate() error {
	validate := validator.New()
	return validate.Struct(req)
}
