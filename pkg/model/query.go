package model

import "github.com/go-playground/validator/v10"

type QueryOptions struct {
	Limit int        `json:"limit"`
	Order QueryOrder `json:"order,omitempty" `
}

type QueryOrder struct {
	Field string `json:"field,omitempty" validate:"omitempty,oneof=age name email  "`
	Order string `json:"order,omitempty" validate:"omitempty,oneof=ASC DESC"`
}

func (qo QueryOptions) Validate() error {
	validate := validator.New()
	return validate.Struct(qo)
}
