package httphelper

import (
	"encoding/json"
	"net/http"
)

func ErrorResponse(w http.ResponseWriter, err error) http.ResponseWriter {
	response, _ := json.Marshal(map[string]string{"error": err.Error()})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(400)
	w.Write(response)
	return w
}
