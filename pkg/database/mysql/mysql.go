package mysql

import (
	"database/sql"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/julioshinoda/vma-backend/pkg/database"
)

type Connection struct{}

func GetDBConn() database.SQLInterface {
	return Connection{}
}

func createConnection() (*sql.DB, error) {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_URL"))
	if err != nil {
		return nil, err
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	return db, nil
}

func (conn Connection) Query(config database.QueryConfig) ([]map[string]interface{}, error) {
	db, err := createConnection()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Query(config.QueryStr, config.Values...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	results := []map[string]interface{}{}
	colNames, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	cols := make([]interface{}, len(colNames))
	colPtrs := make([]interface{}, len(colNames))
	for i := 0; i < len(colNames); i++ {
		colPtrs[i] = &cols[i]
	}
	for rows.Next() {
		result := map[string]interface{}{}
		err = rows.Scan(colPtrs...)
		if err != nil {
			return nil, err
		}
		for i, col := range cols {
			result[colNames[i]] = col
		}
		results = append(results, result)
	}
	return results, nil
}

func (conn Connection) Transaction(config database.QueryConfig) error {
	db, err := createConnection()
	if err != nil {
		return err
	}
	defer db.Close()

	stmtIns, err := db.Prepare(config.QueryStr)
	if err != nil {
		return err
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(config.Values...)
	if err != nil {
		return err
	}
	return nil
}
