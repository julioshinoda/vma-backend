package database

type QueryConfig struct {
	QueryStr string
	Values   []interface{}
}

type SQLInterface interface {
	Query(config QueryConfig) ([]map[string]interface{}, error)
}
