module gitlab.com/julioshinoda/vma-backend

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.0
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/imdario/mergo v0.3.11
	github.com/stretchr/testify v1.4.0
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
)
