.EXPORT_ALL_VARIABLES:

JWT_SECRET=240d61ad666a166f3469f64b953fdbeb8122868d646202477005e3d2964054b4

run: 
	docker-compose up 

stop:
	docker-compose stop

test:
	go test -v ./...

generate-token:
	go run scripts/token/main.go	