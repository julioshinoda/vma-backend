package main

import (
	"fmt"

	"gitlab.com/julioshinoda/vma-backend/pkg/auth"
)

func main() {
	fmt.Println(auth.GenerateToken())
}
